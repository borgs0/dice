class Result {
    constructor(input, operations, total, min = 1, max = null) {
        this.input = input;
        this.operations = operations;
        this.total = total;
        this.min = min;
        this.max = max;
    }
}

module.exports = Result;