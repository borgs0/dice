const Result = require('./Result');

class Operator {
    constructor(operator) {
        this.operator = operator;
        this.operation = null;

        switch (operator) {
            case ("+"):
                this.operation = this.addition;
                break;
            case ("-"):
                this.operation = this.subtraction;
                break;
            default: 
                throw `\`${operator}\` is not a valid operator.`
        }
    }

    static create(str) { return new Operator(str); }

    addition(resultOne, resultTwo) {
        return resultOne.total + resultTwo.total;
    }

    subtraction(resultOne, resultTwo) {
        return resultOne.total - resultTwo.total;
    }
    
    evaluate(resultOne, resultTwo) {
        return new Result(
            `${resultOne.input}${this.operator}${resultTwo.input}`,
            [resultOne, this, resultTwo], 
            this.operation(resultOne, resultTwo));
    }
}

module.exports = Operator;