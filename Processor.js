const DieRollGroup = require('./DieRollGroup/DieRollGroup');
const Operator     = require('./Operator');
const Constant     = require('./Constant')

class Processor {

    static chompValueFunc(content) {
        let match = /^(\D*)(\d*)d(\d+)(([a-z]|[A-Z]|\d)*)\s*/g.exec(content);
        if(match !== null) {
            let num = match[2] == ""? 1 : parseInt(match[2]);
            let size = parseInt(match[3]);
            return [new DieRollGroup(num, size, match[4]), content.substring(match[0].length)];
        }
        match = /^\D*(\d+)\s*/g.exec(content);
        if(match !== null) return [new Constant(parseInt(match[1])), content.substring(match[0].length, content.length)];
        return null;
    }

    static chompOperator(content) {
        let match = /^(\+|\-)(\s*)/g.exec(content);
        if(match !== null) return [new Operator(match[1]), content.substring(match[0].length, content.length)];
        return null;
    }

    static getRolls(content) {
        let rollValue;
        let results = [];
        while ((rollValue = Processor.chompValueFunc(content)) !== null) {
            let result = [rollValue[0]];
            content = rollValue[1];
            
            // Each successive step must consist of:
            //   1. An operator
            //   2. A value function

            let operator = Processor.chompOperator(content);
            if(operator !== null) content = operator[1];
            else {
                results.push(result);
                continue;
            }
            let valueFunc = Processor.chompValueFunc(content);
            if(valueFunc !== null) content = valueFunc[1];
            else {
                results.push(result);
                continue;
            }
            while(operator !== null && valueFunc !== null) {
                result.push(operator[0]);
                result.push(valueFunc[0]);

                operator = null;
                valueFunc = null;

                operator = Processor.chompOperator(content);
                if(operator !== null) content = operator[1];
                valueFunc = Processor.chompValueFunc(content);
                if(valueFunc !== null) content = valueFunc[1];
            }

            results.push(result);
        }

        return results;
    }

    static getResults (str) {
        let combos = Processor.getRolls(str);
        let results = [];

        combos.forEach(combo => {
            let result = null;
            let operator = null;
            for (var i = 0; i < combo.length; i++) {
                let currentOperation = combo[i];
                if(i == 0) {
                    result = currentOperation.evaluate();
                    continue;
                }
                if(i % 2 == 1) {
                    operator = currentOperation;
                    continue;
                }

                result = operator.evaluate(result, currentOperation.evaluate());
            }

            results.push(result);
        });

        return results;
    }
}

module.exports = Processor;
