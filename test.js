const Processor = require('./Processor');

function main() {
    rollAndPrint('     36');
    rollAndPrint('3d6+12');
    rollAndPrint('3d6+2d8');
    rollAndPrint('something 3d6e some stuff at the end');
    rollAndPrint('3d6kl1');
    rollAndPrint('3d6kh2kl1 - 3 +2d12');
    rollAndPrint('something 6d6e and also 12d12');
}

function rollAndPrint(str) {
    let results = Processor.getResults(str);
    console.log(`${str}`);
    results.forEach(result => console.log(result));
}

main();