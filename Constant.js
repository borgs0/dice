const Result = require('./Result');

class Constant {
    constructor(num) {
        this.num = num;
    }

    evaluate() {
        return new Result(`${this.num}`, [this.num], this.num);
    }
}

module.exports = Constant;