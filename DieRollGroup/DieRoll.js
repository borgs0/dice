class DieRoll {
    constructor(dieSize) {
        this.dieSize = dieSize;
        this.rolled = Math.floor(Math.random() * dieSize) + 1;
        this.keep = true;
    }
}

module.exports = DieRoll;