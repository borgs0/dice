const { KeepLowest, KeepHighest, Exploding, RerollArgument } = require('./DieRollGroupArguments');
const DieRoll = require('./DieRoll');
const Result = require('../Result');

class DieRollGroup {  
    constructor(times, size, args) {
        this.times = times;
        this.size = size;
        this.args = args;
        this.rollArguments = [];
        
        if (this.args != null && this.args != '') {
            this.processArgs(this.args);
        }
    }

    processArgs(args) {
        var remainingArgs = args;
        const argumentParsers = [KeepLowest, KeepHighest, Exploding, RerollArgument];

        for (var i = 0; i < argumentParsers.length; i++) {
            var parser = argumentParsers[i];
            let match;
            if ((match = parser.chomp(remainingArgs)) != null) {
                this.rollArguments[this.rollArguments.length] = match.argument;
                remainingArgs = match.remainingArgs;
                i = -1; // potential infinite loop... the halting problem I guess?
            }
        }
    }

    evaluate() {
        var rolls = [];
        for(let i = 0; i < this.times; i ++) {
            rolls[rolls.length] = new DieRoll(this.size);
        }

        for(let i = 0; i < this.rollArguments.length; i++) {
            rolls = this.rollArguments[i].evaluate(rolls);
        }

        var total = rolls.reduce((acc, roll) => acc + (roll.keep? roll.rolled: 0), 0);
        var rolls = rolls.reduce(
            (acc, roll) => {
                acc.push(roll);
                return acc;
            }, []);
        return new Result(`${this.times}d${this.size}${this.args}`, rolls, total);
    }
}

module.exports = DieRollGroup;