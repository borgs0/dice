const DieRoll = require('./DieRoll');

class RollGroupArgument {
    static chomp(str) { throw "\'chomp(str)\' is not implemented for this roll groupargument." }

    evaluate(rolls) { throw "\'evaluate(rolls)\' is not implemented for this roll group argument."}
}

class KeepLowest extends RollGroupArgument {

    constructor(num) {
        super();
        this.num = num;
    }

    static chomp(str) {
        const regex = /^kl(\d*)/gm;

        let match;
        if ((match = regex.exec(str)) == null) {
            return null;
        }

        return {
            remainingArgs: str.slice(match[0].length, str.length),
            argument: new KeepLowest(match[1] == null || match[1] == '' ? 1 : parseInt(match[1]))
        };
    }

    evaluate(rolls) {
        let currentEst = null;
        let found = 0;

        let keepers = rolls.reduce((count, roll) => {
            if (roll.keep) {
                return count + 1;
            }

            return count;
        }, 0);

        // Can't throw away any more.
        if (keepers <= this.num) {
            return rolls;
        }

        var countToThrowAway = keepers - this.num;
        while (found < countToThrowAway) {
            for (var ri = 0; ri < rolls.length; ri++) {
                let r = rolls[ri];
                if (!r.keep) continue;

                if (currentEst == null) {
                    currentEst = r;
                }
                else if (r.value > currentEst.value) {
                    currentEst = r;
                }
            }

            rolls[rolls.indexOf(currentEst)].keep = false;
            currentEst = null;
            found++;
        }


        return rolls;
    }
}

class KeepHighest extends RollGroupArgument {
    constructor(num) {
        super();
        this.num = num;
    }

    static chomp(str) {
        const regex = /^kh(\d*)/gm;
        let match;
        if ((match = regex.exec(str)) == null) {
            return null;
        }

        return {
            remainingArgs: str.slice(match[0].length, str.length),
            argument: new KeepHighest(match[1] == null || match[1] == '' ? 1 : parseInt(match[1]))
        };
    }

    evaluate(rolls) {
        let currentEst = null;
        let found = 0;

        let keepers = rolls.reduce((count, roll) => {
            if (roll.keep) {
                return count + 1;
            }

            return count;
        }, 0);

        // Can't throw away any more.
        if (keepers <= this.num) {
            return rolls;
        }

        var countToThrowAway = keepers - this.num;
        while (found < countToThrowAway) {
            for (var ri = 0; ri < rolls.length; ri++) {
                let r = rolls[ri];
                if (!r.keep) continue;

                if (currentEst == null) {
                    currentEst = r;
                }
                else if (r.value < currentEst.value) {
                    currentEst = r;
                }
            }

            rolls[rolls.indexOf(currentEst)].keep = false;
            currentEst = null;
            found++;
        }


        return rolls;
    }
}

class Exploding extends RollGroupArgument {
    static chomp(str) {
        const regex = /^e/gm;
        let match;
        if ((match = regex.exec(str)) == null) {
            return null;
        }

        return {
            remainingArgs: str.slice(match[0].length, str.length),
            argument: new Exploding()
        };
    }

    evaluate(rolls) {
        var dieSize = rolls[0].dieSize;
        var maxCount = 0;
        rolls.forEach(r => {
            if (r.keep && r.value == dieSize) {
                maxCount++;
            }
        })

        for (var i = 0; i < maxCount; i++) {
            let newRoll = new DieRoll(dieSize);
            if (newRoll.value == dieSize) {
                maxCount++;
            }
            rolls.push(newRoll);
        }

        return rolls
    }
}

class RerollArgument extends RollGroupArgument {
    constructor(num) {
        super();
        this.num = num;
    }

    static chomp(str) {
        let regex = /^r(\d+)/gm;
        let match;
        if ((match = regex.exec(str)) == null) {
            return null;
        }

        return {
            remainingArgs: str.slice(match[0].length, str.length),
            argument: new RerollArgument(match[1])
        };
    }

    evaluate(rolls) {
        let newRolls = [];
        for (var ri = 0; ri < rolls.length; ri++) {
            if (rolls[ri].value == this.num) {
                newRolls.push(new DieRoll(rolls[ri].dieSize));
                rolls[ri].keep = false;
            }
        }

        newRolls.forEach(r => rolls.push(r));
        return rolls;
    }
}

module.exports = { KeepLowest, KeepHighest, Exploding, RerollArgument };